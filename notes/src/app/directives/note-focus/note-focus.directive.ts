import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appNoteFocus]'
})
export class NoteFocusDirective {

  constructor(private _hostElement: ElementRef<HTMLElement>) {}

  @HostListener('document:click', ['$event'])
  public onNoteClicked(event: MouseEvent) {
    let target = event.target as HTMLElement;

    if (this._hostElement.nativeElement.contains(target))
      this._hostElement.nativeElement.classList.add('focus');
    else
      this._hostElement.nativeElement.classList.remove('focus');
  }

}
