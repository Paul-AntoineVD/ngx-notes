export interface Note {
  id: string;
  title: string;
  content: string;
  author: string;
  creationDate: string;
  lastEditionDate: string;
}

