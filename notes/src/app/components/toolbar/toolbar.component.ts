import { Component, EventEmitter, Output } from '@angular/core';

export type SortType = 'az' | 'za';

export interface Filter {
  value: string;
  sort: SortType;
}

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  @Output() filterEvent: EventEmitter<Filter> = new EventEmitter<Filter>();

  public filterValue: string = '';
  public sortValue: SortType = 'az';

  public sortList: SortType[] = ['az', 'za'];
  private currentSort = 0;


  public updateSort() {
    this.currentSort++;

    if (this.currentSort > this.sortList.length-1)
      this.currentSort = 0;

    this.sortValue = this.sortList[this.currentSort];
  }

}
