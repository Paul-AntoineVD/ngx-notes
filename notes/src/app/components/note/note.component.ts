import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { Note } from 'src/app/models/note.model';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],

})
export class NoteComponent {
  @Input() note: Note | null = null;
  @Output() deleteNoteEvent: EventEmitter<Note> = new EventEmitter();

  public editable: boolean = false;

  public triggerEdition(): void {
    this.editable = !this.editable;
  }

  public deleteNote(): void {
    if (this.note) {
      this.deleteNoteEvent.emit(this.note);
    }
  }
}
