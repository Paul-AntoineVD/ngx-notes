import { Component, OnInit } from '@angular/core';
import { Note } from 'src/app/models/note.model';
import { v4 as uuidv4 } from 'uuid';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss'],
  animations: [
    trigger('animateDestroy', [
      state('void', style({opacity: '0'})),
      transition('* => void', animate('500ms ease'))
    ])
  ]
})
export class NoteListComponent implements OnInit {

  public notes: Note[] = [];

  public ngOnInit(): void {
    this.notes = [
      {
        author: 'George Lucas',
        content: 'Il y a longtemps dans une galaxie lointaine',
        title: 'La menace phantome',
        creationDate: '2001',
        id: '1',
        lastEditionDate: '2001'
      },
      {
        author: 'George Lucas',
        content: 'Il y a longtemps dans une galaxie lointaine',
        title: 'La guerre des clones',
        creationDate: '2003',
        id: '2',
        lastEditionDate: '2003'
      },
      {
        author: 'George Lucas',
        content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad earum eveniet excepturi minima eaque voluptate officia, unde molestias fugiat beatae. Quas ratione laborum ab natus temporibus magni unde eos reprehenderit.',
        title: 'La revanche des sith',
        creationDate: '2005',
        id: '3',
        lastEditionDate: '2005'
      }
    ]
  }

  public deleteNote(noteToDelete: Note) {
    this.notes = this.notes.filter(note => note.id != noteToDelete.id);
  }

  public addNote() {
    this.notes = [...this.notes, {
      id: uuidv4(),
      author: 'None',
      content: 'Empty...',
      creationDate: '',
      lastEditionDate: '',
      title: 'New note'
    }];
  }
}
